//
//  CollectionViewCell.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 7/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

var smCellSize: CGSize {
    get{
        let w = UIScreen.main.bounds.width * (1.0/3.0) - 1;
        let h = (UIScreen.main.bounds.width * (2.0/3.0) - 1) * 0.5;
        return CGSize(width: w, height: h);
    }
};
var mdCellSize: CGSize {
    get{
        let w = UIScreen.main.bounds.width * (2.0/3.0) - 1;
        return CGSize(width: w, height: w);
    }
};
var lgCellSize: CGSize {
    get{
        let w = UIScreen.main.bounds.width * (3.0/3.0) - 1;
        return CGSize(width: w, height: w);
    }
};

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var imageView: UIImageView!
    
}

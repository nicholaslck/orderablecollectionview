//
//  CollectionViewReorderableLayout.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 7/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit
protocol CollectionViewDelegateGridLayout: UICollectionViewDelegate {
    
    func collectionView(_ collectionView:UICollectionView, horizontalSpacingForSectionAtIndex section:Int) -> CGFloat;
    
    func collectionView(_ collectionView:UICollectionView, verticalSpacingForSectionAtIndex section:Int) -> CGFloat;
    
    func collectionView(_ collectionView:UICollectionView, sizeForCellAtIndexPath indexPath:IndexPath) -> CGSize;
    
}

class CollectionViewGridLayout: UICollectionViewLayout {

    var delegate: CollectionViewDelegateGridLayout? {
        return self.collectionView?.delegate as? CollectionViewDelegateGridLayout;
    }
    
    private var contentHeight: CGFloat = 0;
    private var contentWidth: CGFloat {
        get {
            if let collectionView = collectionView {
                let insets = collectionView.contentInset
                return collectionView.bounds.width - insets.left - insets.right;
            }
            return 0;
        }
    }
    
    override var collectionViewContentSize: CGSize {
        get {
            return CGSize(width: self.contentWidth, height: self.contentHeight);
        }
    }
    
    private let defaultCellSize: CGSize = CGSize(width: 50, height: 50);
    
    var xSpacing: CGFloat = 0;
    var ySpacing: CGFloat = 0;
    
    var cache = [UICollectionViewLayoutAttributes]();
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        return true;
    }
    
    override func prepare() {
        super.prepare();
        cache.removeAll();
        ySpacing = self.delegate?.collectionView(self.collectionView!, verticalSpacingForSectionAtIndex: 0) ?? 18;
        xSpacing = self.delegate?.collectionView(self.collectionView!, horizontalSpacingForSectionAtIndex: 0) ?? 18;
        
        // prepare grid coordinate
        let numberOfColumn = 3;
        let columnWidth = contentWidth / CGFloat(numberOfColumn);
        var offsetY = [CGFloat](repeating: ySpacing, count: numberOfColumn);
        var offsetX = [CGFloat]();
        for col in 0..<numberOfColumn {
            offsetX.append(CGFloat(col) * columnWidth);
        }
        
        for item in 0 ..< collectionView!.numberOfItems(inSection: 0) {
            let indexPath = IndexPath(item: item, section: 0);
            let cellSize: CGSize = self.delegate?.collectionView(self.collectionView!, sizeForCellAtIndexPath: indexPath) ?? defaultCellSize;
            assert(cellSize.width <= contentWidth, String(format: "Size of cell at indexPath %@ must within the collection content width.", indexPath as NSIndexPath));
            
            let requiredColumn = Int(ceil(cellSize.width / columnWidth));
            let minOffsetY = offsetY.min() ?? 0;
            var column = offsetY.index(of: minOffsetY) ?? 0;
            
            // Ensure cell can show completely inside contentWidth. Top-Left-aligned.
            switch requiredColumn {
            case 0, 1:
                break;
            case 2:
                switch column {
                case 0:
                    offsetY[0] = offsetY[1];
                case 1:
                    if (offsetY[2] > offsetY[1]) {
                        column = 0;
                    }
                case 2:
                    if (offsetY[0] > offsetY[1]) {
                        column = 1;
                    }
                    else{
                        column = 0;
                        offsetY[0] = offsetY[1];
                    }
                default:
                    break;
                }
            case 3:
                column = 0;
                offsetY[0] = offsetY.max() ?? 0;
            default:
                assert(cellSize.width <= contentWidth, String(format: "Size of cell at indexPath %@ must within the collection content width.", indexPath as NSIndexPath));
                return;
            }
            
            // Config frame position
            let frame = CGRect(x: offsetX[column], y: offsetY[column], width: cellSize.width, height: cellSize.height);
            
            let insetFrame = frame.insetBy(dx: xSpacing*0.5, dy: ySpacing*0.5);
            
            let attr = UICollectionViewLayoutAttributes(forCellWith: indexPath);
            attr.frame = insetFrame;
            attr.zIndex = 0
            cache.append(attr);
            
            // update next y coordinate
            for col in 0..<numberOfColumn {
                let colRect = CGRect(x: offsetX[col], y: frame.origin.y, width: columnWidth, height: frame.size.height);
                if (frame.intersects(colRect)) {
                    offsetY[col] = frame.maxY;
                }
            }
            
            // update contentSize
            contentHeight = max(offsetY[0], offsetY[1], offsetY[2]);
        }
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var layoutAttributesArray = [UICollectionViewLayoutAttributes]()
        
        for attr in cache {
            if rect.intersects(attr.frame) {
                layoutAttributesArray.append(attr)
            }
        }
        
        return layoutAttributesArray
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item];
    }
    
    // Custom animation
    
    var moveUpdateItems:[UICollectionViewUpdateItem] = []
    
    var insertIndexPaths: [IndexPath] = []
    var deleteIndexPaths: [IndexPath] = []
    
    override func prepare(forCollectionViewUpdates updateItems: [UICollectionViewUpdateItem]) {
        super.prepare(forCollectionViewUpdates: updateItems)
        print("prepare update");
        updateItems.forEach { (updateItem) in
            // If the update item's index path has an "item" value of NSNotFound, it means it was a section update, not an individual item.
            // This is 100% undocumented but 100% reproducible.
            if updateItem.indexPathBeforeUpdate?.item != NSNotFound && updateItem.indexPathAfterUpdate?.item != NSNotFound {
                if updateItem.updateAction == .move {
                    moveUpdateItems.append(updateItem)
                }
                else if updateItem.updateAction == .insert {
                    if let indexPath = updateItem.indexPathAfterUpdate {
                        insertIndexPaths.append(indexPath)
                    }
                }
                else if updateItem.updateAction == .delete {
                    if let indexPath = updateItem.indexPathBeforeUpdate {
                        deleteIndexPaths.append(indexPath)
                    }
                }
            }
        }
    }
    
    override func finalizeCollectionViewUpdates() {
        super.finalizeCollectionViewUpdates();
        moveUpdateItems.removeAll()
        insertIndexPaths.removeAll()
        deleteIndexPaths.removeAll()
        print("finished update")
    }
    
    override func finalLayoutAttributesForDisappearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        //        print("item \(itemIndexPath.item) willDisappear")
        var attr = super.finalLayoutAttributesForDisappearingItem(at: itemIndexPath)
        
        if moveUpdateItems.count > 0 {
            attr = self.cache[itemIndexPath.item]
//            attr?.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
//            attr?.alpha = 1;
        }
        else {
            if deleteIndexPaths.contains(itemIndexPath) {
                attr = self.cache[itemIndexPath.item]
                attr?.transform = CGAffineTransform.identity.rotated(by: CGFloat.pi)
            }
        }
        
        return attr
    }
    
    override func initialLayoutAttributesForAppearingItem(at itemIndexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        var attr = super.initialLayoutAttributesForAppearingItem(at: itemIndexPath)
        
        if moveUpdateItems.count > 0 {
            attr = self.cache[itemIndexPath.item]
//            attr?.transform = CGAffineTransform.identity.rotated(by: -CGFloat.pi)
//            attr?.alpha = 0;
        }
        else {
            if insertIndexPaths.contains(itemIndexPath) {
                attr = self.cache[itemIndexPath.item]
                attr?.transform = CGAffineTransform.identity.scaledBy(x: 3.0, y: 1.2)
                attr?.alpha = 0;
            }
        }
        return attr
    }
    
    
}

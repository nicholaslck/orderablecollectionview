//
//  ViewController.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 8/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func shouldPerformSegue(withIdentifier identifier: String, sender: Any?) -> Bool
    {
        if identifier == "advanceRoute" {
            guard #available(iOS 11.0, *) else {
                
                let ok = UIAlertAction(title: "OK", style: .default, handler: nil);
                let alert = UIAlertController(title: "OS version not supported", message: "Please update to iOS 11.0 or above to use advance drag and drop function.", preferredStyle: .alert)
                alert.addAction(ok);
                self.present(alert, animated: true, completion: nil);
                
                return false;
            }
        }
        return true;
    }

}

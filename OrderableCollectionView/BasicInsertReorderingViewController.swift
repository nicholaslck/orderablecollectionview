//
//  BasicInsertReorderingViewController.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 13/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

fileprivate let count: Int = 15;

class BasicInsertReorderingViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    private var holdGesture: UILongPressGestureRecognizer!;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.collectionView.delegate = self;
        self.collectionView.dataSource = self;
        // Do any additional setup after loading the view, typically from a nib.
        self.dataList = ColorModel.arrayOfRandomColorModel(count:count);
        self.holdGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleHoldGesture(gesture:)));
        self.collectionView.addGestureRecognizer(self.holdGesture);
    }
    
    var dataList: Array<ColorModel> = [];
    
    var isReordering = false;
    
    @objc private func handleHoldGesture(gesture: UILongPressGestureRecognizer!) {
        switch gesture.state {
        case .began:
            if (!self.isReordering)
            {
                if let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) {
                    self.isReordering = self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath);
                }
            }
        case .changed:
            self.collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: self.collectionView));
        case .ended:
            self.collectionView.endInteractiveMovement();
            self.isReordering = false;
        default:
            self.collectionView.cancelInteractiveMovement();
            self.isReordering = false;
        }
    }
    
    func printArray() {
        var newArr: Array<Int> = [];
        for index in 0..<self.dataList.count {
            newArr.append(self.dataList[index].number);
        }
        print("new list: \(newArr)");
    }
}

extension BasicInsertReorderingViewController : CollectionViewDelegateGridLayout, UICollectionViewDataSource {
    
    // MARK:- Custom CollectionViewDelegateGridLayout
    func collectionView(_ collectionView: UICollectionView, horizontalSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, verticalSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, sizeForCellAtIndexPath indexPath: IndexPath) -> CGSize {
        switch indexPath.item {
        case 0, 8:
            return mdCellSize;
        case 6:
            return CGSize(width: lgCellSize.width, height: smCellSize.height);
        default:
            return smCellSize;
        }
    }
    
    // MARK:- CollectionView Data source and delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataList.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CollectionViewCell;
        
        cell.backgroundColor = self.dataList[indexPath.item].color;
        cell.titleLabel.text = String(format: "%d", self.dataList[indexPath.item].number);
        cell.titleLabel.textColor = UIColor.black;
        cell.imageView.image = self.dataList[indexPath.item].image;
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10;
    }
    
    // MARK: - Drag and drop
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        switch indexPath.item {
        case 0, 6:
            return false;
        default:
            return true;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        let data = self.dataList[sourceIndexPath.item];
        self.dataList.remove(at: sourceIndexPath.item);
        self.dataList.insert(data, at: destinationIndexPath.item);
        
        print("start index: \(sourceIndexPath.item)");
        print("end index: \(destinationIndexPath.item)");
        self.printArray();
    }
}

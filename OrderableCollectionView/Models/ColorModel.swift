//
//  ColorModel.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 7/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

class ColorModel: NSObject {
    
    private override init() {
        super.init();
    }
    
    public convenience init(withNumber num: Int) {
        self.init();
        self.number = num;
        
        self.color = UIColor(red: CGFloat(drand48()), green: CGFloat(drand48()), blue: CGFloat(drand48()), alpha: CGFloat(1.0));
    }
    
    public convenience init(withNumber num: Int, color: UIColor) {
        self.init(withNumber: num);
        self.color = color;
    }
    
    private(set) public var number: Int = 0;
    private(set) public var color: UIColor = UIColor.blue;
    public var type: Int = 0;
    public var image: UIImage? {
        get {
            let num = self.type == 0 ? self.number%4 : self.type;
            return UIImage.init(named: String(format: "img_%d", num));
        }
    }
}

// MARK: - Model Factory
extension ColorModel {
    static func arrayOfRandomColorModel(count: Int) -> Array<ColorModel> {
        var arr: Array<ColorModel> = [];
        
        for index in 0..<count {
            let color = ColorModel.init(withNumber: index);
            color.type = index % 4
            arr.append(color);
        }
        
        return arr;
    }
    
    static func arrayOfRandomColorModelWithTypeAssigned() -> [ColorModel] {
        var arr: [ColorModel] = [];
        
        let countType = 6
        var startIndex = 0
        
        for i in startIndex..<12 {
            let color = ColorModel.init(withNumber: i)
            color.type = 0
            arr.append(color)
        }
        startIndex += 12
        
        for i in startIndex..<(startIndex + countType) {
            let color = ColorModel.init(withNumber: i)
            color.type = 1
            arr.append(color)
        }
        startIndex += countType
        
        for i in startIndex..<(startIndex + countType) {
            let color = ColorModel.init(withNumber: i)
            color.type = 2
            arr.append(color)
        }
        startIndex += countType
        
        for i in startIndex..<(startIndex + countType) {
            let color = ColorModel.init(withNumber: i)
            color.type = 3
            arr.append(color)
        }
        startIndex += countType
        
        return arr
    }
}

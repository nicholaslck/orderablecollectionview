//
//  AdvanceReorderViewController.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 7/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

fileprivate let count = 23;

class AdvanceReorderViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataList = ColorModel.arrayOfRandomColorModel(count: count);
        if #available(iOS 11.0, *) {
            self.collectionView.dragDelegate = self
            self.collectionView.dropDelegate = self;
            self.collectionView.dragInteractionEnabled = true;
            self.collectionView.reorderingCadence = .immediate;
        };
    }
    
    var dataList: Array<ColorModel> = [];
    
    func printArray() {
        var newArr: Array<Int> = [];
        for index in 0..<self.dataList.count {
            newArr.append(self.dataList[index].number);
        }
        print("new list: \(newArr)");
    }
    
}

extension AdvanceReorderViewController : UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CollectionViewCell;
        let data = self.dataList[indexPath.item];
        
        cell.titleLabel.text = String(format: "%d", data.number);
        cell.backgroundColor = data.color;
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataList.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 80.0, height: 80.0);
    }
}

// MARK: - UICollectionViewDragDelegate
@available(iOS 11.0, *)
extension AdvanceReorderViewController : UICollectionViewDragDelegate {
    
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        
        let item = self.dataList[indexPath.item];
        let dragItem = UIDragItem(itemProvider: NSItemProvider());
        dragItem.localObject = item;
        
        return [dragItem];
    }
    
    func collectionView(_ collectionView: UICollectionView, dragSessionAllowsMoveOperation session: UIDragSession) -> Bool {
        return true;
    }
    
}

// MARK: - UICollectionViewDropDelegate
@available(iOS 11.0, *)
extension AdvanceReorderViewController : UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        switch coordinator.proposal.operation {
        case .move:
            if var desIndexPath = coordinator.destinationIndexPath {
                if (desIndexPath.item >= self.dataList.count) {
                    desIndexPath = IndexPath(item: self.dataList.count - 1, section: desIndexPath.section);
                }
                if let item = coordinator.items.first {
                    if let srcIndexPath = item.sourceIndexPath {
                        
                        let data = self.dataList[srcIndexPath.item];
                        collectionView.performBatchUpdates({
                            self.dataList.remove(at: srcIndexPath.item);
                            self.dataList.insert(data, at: desIndexPath.item);
                            collectionView.deleteItems(at: [srcIndexPath]);
                            collectionView.insertItems(at: [desIndexPath]);
                            self.printArray();
                        }, completion: nil);
                        coordinator.drop(item.dragItem, toItemAt: desIndexPath);
                        
                    }
                }
            }
        default:
            return;
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        if let _ = session.localDragSession {
            if collectionView.hasActiveDrag {
                return UICollectionViewDropProposal(operation: .move, intent:.insertAtDestinationIndexPath);
            }
        }
        return UICollectionViewDropProposal(operation: .forbidden, intent:.insertAtDestinationIndexPath);
    }
    
}



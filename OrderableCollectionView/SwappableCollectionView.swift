//
//  SwappableCollectionView.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 11/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

private enum Direction {
    case up
    case down
}

class SwappableCollectionView: UICollectionView {
 
    var interactiveIndexPath: IndexPath?;
    var interactiveView: UIView?;
    var interactiveCell: UICollectionViewCell?;
    
    var targetIndexPath: IndexPath?;
    
    override func dequeueReusableCell(withReuseIdentifier identifier: String, for indexPath: IndexPath) -> UICollectionViewCell {
        let cell = super.dequeueReusableCell(withReuseIdentifier: identifier, for: indexPath);
        cell.isHidden = (indexPath.item == self.interactiveIndexPath?.item);
        return cell;
    }
    
    override func beginInteractiveMovementForItem(at indexPath: IndexPath) -> Bool {
        
        let interactiveIndexPathOkToMove = self.dataSource?.collectionView!(self, canMoveItemAt: indexPath);
        if (interactiveIndexPathOkToMove ?? false)
        {
            self.interactiveIndexPath = indexPath;
            self.targetIndexPath = indexPath;
            self.interactiveCell = self.cellForItem(at: indexPath);
            self.interactiveCell?.isHidden = true;
            self.interactiveView = (self.interactiveCell as? CollectionViewCell)?.snapshotView(afterScreenUpdates: false);
            
            self.interactiveView?.alpha = 0.95;
            self.interactiveView?.frame = self.interactiveCell!.frame;
            
            self.addSubview(self.interactiveView!);
            self.bringSubview(toFront: self.interactiveView!);
            
            UIView.animate(withDuration: 0.3, animations: {
                self.interactiveView?.frame = self.interactiveCell!.frame.insetBy(dx: -5, dy: -5);
            });
            return true;
        }
        self.cleanup();
        return false;
    }
    
    override func updateInteractiveMovementTargetPosition(_ targetPosition: CGPoint) {
        self.interactiveView?.center = targetPosition;
        
        self.targetIndexPath = self.indexPathForItem(at: targetPosition);
        
        // config auto scroll
        let bottomDetector = CGRect(x: 0, y: self.bounds.maxY, width: self.bounds.width, height: 1);
        let topDetector = CGRect(x: 0, y: self.bounds.minY-1, width: self.bounds.width, height: 1);
        
        if (self.interactiveView?.frame.intersects(bottomDetector)) ?? false {
            self.startScrollingTimer(direction: .down);
        }
        else if (self.interactiveView?.frame.intersects(topDetector)) ?? false {
            self.startScrollingTimer(direction: .up);
        }
        else{
            self.stopTimer()
        }
    }
    
    override func endInteractiveMovement() {
        self.stopTimer();
        if let targetIndexPath = self.targetIndexPath, let interactiveIndexPath = self.interactiveIndexPath {
            
//            print("source cell \(String(describing: self.cellForItem(at: interactiveIndexPath)))");
//            print("target cell \(String(describing: self.cellForItem(at: targetIndexPath)))");
            
            let targetOkToMove = self.dataSource?.collectionView!(self, canMoveItemAt: targetIndexPath) ?? false;
            if (targetOkToMove && targetIndexPath.item != interactiveIndexPath.item)
            {
                UIView.animateKeyframes(withDuration: 0.3, delay: 0, options: .calculationModeLinear, animations: {
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1, animations: {
                        self.performBatchUpdates({
                            
                            self.moveItem(at: targetIndexPath, to: interactiveIndexPath);
                            self.moveItem(at: interactiveIndexPath, to: targetIndexPath);
                        }) { (finished) in
                        }
                    });
                    UIView.addKeyframe(withRelativeStartTime: 0.0, relativeDuration: 1, animations: {
                        self.interactiveView?.frame = (self.cellForItem(at: targetIndexPath)?.frame)!
                        self.interactiveView?.alpha = 1.0;
                    });
                }) { (finished) in
                    self.interactiveIndexPath = nil;
                    self.targetIndexPath = nil;
                    self.dataSource?.collectionView!(self, moveItemAt: interactiveIndexPath, to: targetIndexPath);
                    self.reloadItems(at: [interactiveIndexPath, targetIndexPath]);
                    self.cleanup();
                }
            }
            else {
                self.cleanup();
            }
        }
        else {
            self.cleanup();
        }
    }
    
    override func cancelInteractiveMovement() {
        self.cleanup();
    }
    
    private func cleanup() {
        self.stopTimer();
        self.interactiveView?.layer.removeAllAnimations();
        self.interactiveView?.removeFromSuperview();
        self.interactiveView = nil;
        self.interactiveCell?.isHidden = false;
        self.interactiveCell = nil;
        self.interactiveIndexPath = nil;
        self.targetIndexPath = nil;
    }
    
    // Scrolling
    var timer: Timer?;
    
    private func startScrollingTimer(direction: Direction) {
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 0.03, target: self, selector: #selector(timerLoopAction(timer:)), userInfo: direction, repeats: true);
        }
        timer?.fire();
    }
    
    private func stopTimer() {
        timer?.invalidate();
        self.timer = nil;
    }
    
    @objc private func timerLoopAction(timer: Timer) {
        let direction = timer.userInfo as! Direction;
        var offsetY = CGFloat(0);
        var nextOffset: CGPoint = self.contentOffset;
        
        switch direction {
        case .up:
            offsetY = CGFloat(-10);
            nextOffset = CGPoint(x: self.contentOffset.x, y: floor(max((self.contentOffset.y + offsetY),0)));
        case .down:
            offsetY = CGFloat(10);
            nextOffset = CGPoint(x: self.contentOffset.x, y: floor(min((self.contentOffset.y + offsetY), self.contentSize.height - self.bounds.height)));
        }
        
        if let interactiveView = self.interactiveView {
            if (nextOffset.y != self.contentOffset.y)
            {
                self.contentOffset = nextOffset;
                interactiveView.center = CGPoint(x: interactiveView.frame.midX, y: interactiveView.frame.midY + offsetY);
            }
        }
    }
}

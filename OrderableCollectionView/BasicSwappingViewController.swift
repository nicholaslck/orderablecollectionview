//
//  BasicReorderViewController.swift
//  OrderableCollectionView
//
//  Created by Nicholas LAU on 6/6/2018.
//  Copyright © 2018 TFI Digital Media Limited. All rights reserved.
//

import UIKit

let cellIdentifier: String = "cell";
fileprivate let count: Int = 60;

class BasicSwappingViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    private var holdGesture: UILongPressGestureRecognizer!;
    var originalSourceList: [ColorModel] = []
    var dataList: [ColorModel] = []
    var isReordering = false;
    var shakeCount = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.originalSourceList = ColorModel.arrayOfRandomColorModelWithTypeAssigned()
        self.dataList = self.originalSourceList;
        self.holdGesture = UILongPressGestureRecognizer(target: self, action: #selector(handleHoldGesture(gesture:)));
        self.collectionView.addGestureRecognizer(self.holdGesture);
    }
    
    @objc private func handleHoldGesture(gesture: UILongPressGestureRecognizer!) {
        switch gesture.state {
        case .began:
            if (!self.isReordering)
            {
                if let selectedIndexPath = self.collectionView.indexPathForItem(at: gesture.location(in: self.collectionView)) {
                    self.isReordering = self.collectionView.beginInteractiveMovementForItem(at: selectedIndexPath);
                }
            }
        case .changed:
            self.collectionView.updateInteractiveMovementTargetPosition(gesture.location(in: self.collectionView));
        case .ended:
            self.collectionView.endInteractiveMovement();
            self.isReordering = false;
        default:
            self.collectionView.cancelInteractiveMovement();
            self.isReordering = false;
        }
    }
    
    func printArray() {
        var newArr: Array<Int> = [];
        for index in 0..<self.dataList.count {
            newArr.append(self.dataList[index].number);
        }
        print("new list: \(newArr)");
    }
    
    override func motionBegan(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            self.handleShakeBegin()
        }
    }
    
    override func motionEnded(_ motion: UIEventSubtype, with event: UIEvent?) {
        if motion == .motionShake {
            self.handleShakeEnd()
        }
    }

}

extension BasicSwappingViewController : CollectionViewDelegateGridLayout, UICollectionViewDataSource {
    
    // MARK:- Custom CollectionViewDelegateGridLayout
    func collectionView(_ collectionView: UICollectionView, horizontalSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, verticalSpacingForSectionAtIndex section: Int) -> CGFloat {
        return 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, sizeForCellAtIndexPath indexPath: IndexPath) -> CGSize {
        switch indexPath.item {
        case 0, 8:
            return mdCellSize;
        case 6:
            return CGSize(width: lgCellSize.width, height: smCellSize.height);
        default:
            return smCellSize;
        }
    }
    
    // MARK:- CollectionView Data source and delegate
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.dataList.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell: CollectionViewCell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier, for: indexPath) as! CollectionViewCell;
        
        cell.backgroundColor = self.dataList[indexPath.item].color;
        cell.titleLabel.text = String(format: "%d", self.dataList[indexPath.item].number);
        cell.titleLabel.textColor = UIColor.black;
        cell.imageView.image = self.dataList[indexPath.item].image;
        
        return cell;
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.layer.cornerRadius = 10;
    }
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.layer.zPosition = 0;
    }
    
    // MARK: - Drag and drop
    func collectionView(_ collectionView: UICollectionView, canMoveItemAt indexPath: IndexPath) -> Bool {
        switch indexPath.item {
        case 0, 6:
            return false;
        default:
            return true;
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, moveItemAt sourceIndexPath: IndexPath, to destinationIndexPath: IndexPath) {
        self.dataList.swapAt(sourceIndexPath.item, destinationIndexPath.item);
        
        print("start index: \(sourceIndexPath.item)");
        print("end index: \(destinationIndexPath.item)");
        self.printArray();
    }
}

extension BasicSwappingViewController {
    
    private func handleShakeBegin() {
    }
    
    private func handleShakeEnd() {
        let type = (self.shakeCount + 1) % 4
        self.shakeCount = type
        
        if type == 0 {
            self.dataList = self.originalSourceList
            self.collectionView.reloadData()
            return
        }
        
        var newList: [ColorModel] = []
        // extract all selected type
        
        var originalIndexList: [Int] = []
        for i in 0..<self.dataList.count {
            let model = self.dataList[i]
            if model.type == type {
                originalIndexList.append(i)
            }
        }
        
        let targetIndexList = Array(0..<originalIndexList.count)
        
        newList = self.dataList
        for i in 0..<originalIndexList.count {
            newList.swapAt(originalIndexList[i], targetIndexList[i])
        }
        
        print(originalIndexList)
        print(targetIndexList)
        print(newList.map({ model in model.number}))
        print("start Animation")
        
        // perform animation
        self.dataList = newList
        self.collectionView.performBatchUpdates({
            
            for i in 0..<originalIndexList.count {
                let sourceIndexPath = IndexPath(item: originalIndexList[i], section: 0)
                let targetIndexPath = IndexPath(item: targetIndexList[i], section: 0)
                self.collectionView.moveItem(at: targetIndexPath, to: sourceIndexPath)
                self.collectionView.moveItem(at: sourceIndexPath, to: targetIndexPath)
                
            }

        }) { (completed) in
            if completed {

                print("completed Animation")
                
            }
        }
    }
}






























































